var mxStack = function () {};
mxStack['create'] = function () {
        return {
            stack: [],
            push: function (mx) {
                var copy = mat4.create();
                mat4.set(mx, copy);
                this.stack.push(copy);
            },
            pop: function () {
                if (this.stack.length == 0) {
                    throw "Invalid popMatrix!";
                }
                return this.stack.pop();
            },
        };
    };
